CREATE TABLE config (
  id INT NOT NULL,
  website text NOT NULL,
  url text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE visitor (
  id INT NOT NULL,
  counter INT NOT NULL,
  cookie text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE tracking (
  id INT NOT NULL,

  visitor_id INT NOT NULL,
  remoteAddr text NOT NULL, -- more important
  userAgent text NOT NULL,  -- more important
  referer text NOT NULL     -- more important info

) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- config
ALTER TABLE `config`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

-- visitor
ALTER TABLE `visitor`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `visitor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
-- tracking
ALTER TABLE `tracking`
  ADD PRIMARY KEY  (`id`),
  ADD KEY (`visitor_id`);

ALTER TABLE `tracking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT; 

-- add header columns to tracking 
ALTER TABLE `tracking` ADD COLUMN connection text;
ALTER TABLE `tracking` ADD COLUMN cacheControl text;
ALTER TABLE `tracking` ADD COLUMN accept text;
ALTER TABLE `tracking` ADD COLUMN acceptEncoding text;
ALTER TABLE `tracking` ADD COLUMN cookie text;
ALTER TABLE `tracking` ADD COLUMN userAgent text;
ALTER TABLE `tracking` ADD COLUMN acceptLanguage text;
ALTER TABLE `tracking` ADD COLUMN referer text;


